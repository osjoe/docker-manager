<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<title>Docker容器管理</title>
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<style>
.table1 {
	width: 100%;
}

.table1 th {
	background-color: #0033dd;
	color: #fff
}

.table1, .table1 th, .table1 td {
	font-size: 0.95em;
	text-align: center;
	padding: 10px;
	border: 1px solid #6fcdfe;
	border-collapse: collapse
}

.table1 tr:nth-child(odd) {
	background-color: #dbf2fe;
}

.table1 tr:nth-child(even) {
	background-color: #fdfdfd;
}

.div_content {
	display: none;
	position: absolute;
	top: 25%;
	left: 40%;
	width: 55%;
	height: 30%;
	padding: 20px;
	border: 1px solid #0066cc;
	background-color: white;
	z-index: 1002;
	overflow: auto;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#toadd").click(function() {
			$("#form").show();
		});

		$("#close").click(function() {
			$("#form").hide();
		});

		$("#add").click(function() {
			var photo = $("#war").val();
			if ("" == photo) {
				alert("请选择要部署应用war包...");
				return false;
			} else {
				var options = {
					url : "/add_container",
					type : "POST",
					cache : false,
					dataType : "json",
					success : function(data) {
						alert(data.msg);
						self.location = "/";
					},
					error : function(a, b, c) {
						console.log([ a, b, c ]);
					}
				}
				$('#addContainerForm').ajaxSubmit(options);
			}
		});

		//定时刷新监控结果，也可用websocket来推送
		setInterval(stat, 1000);
		function stat() {
			var options_stat = {
				url : "/stats",
				type : "get",
				cache : false,
				data : null,
				dataType : "json",
				success : function(data) {
					$("#stats").html("<pre>" + data.msg + "</pre>");
				},
				error : function(a, b, c) {
					console.log([ "error:", a, b, c ]);
				}
			}
			$.ajax(options_stat);
		}
	});
</script>
</head>
<html>
<body>
	<div style="width: 90%; margin: 0 auto;">
	<!-- docker 信息 -->
	<div>
		<h3>Docker服务信息</h3>
		<div style="width: 100%">
			<table class="table1">
			 <tr>
			 <td><b>镜像数</b></td><td>${dockerInfo.images }</td>
			 <td><b>CPU核数</b></td><td>${dockerInfo.cpus }</td>
			 <td><b>内存大小</b></td><td>${dockerInfo.totalMem/1024/1024/1024 }GB</td>
			 </tr>
			</table>
		</div>
	</div>
	<!-- 容器列表 -->
		<div style="float: left;">
			<h3>Docker容器列表</h3>
		</div>
		<div style="float: right;margin-top:20px">
			<button id="toadd" style="padding: 5px">添加Tomcat容器</button>
		</div>
		<table class="table1">
			<thead>
				<tr>
					<th class="center">序号</th>
					<th class="center">容器id</th>
					<th class="center">容器名</th>
					<th class="center">容器状态</th>
					<th class="center">镜像名</th>
					<th class="center">外部端口</th>
					<th class="center">内部端口</th>
					<th class="center">CPU限额策略</th>
					<th class="center">内存限额</th>
					<th class="center">应用访问地址</th>
					<th class="center">操作</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="c" items="${cls}" varStatus="vs">
					<tr>
						<td align="center">${vs.index + 1}</td>
						<td align="center">${c.id}</td>
						<td align="center">${c.name}</td>
						<td align="center">${c.status}</td>
						<td align="center">${c.imageName}</td>
						<td align="center">${c.pubPort}</td>
						<td align="center">${c.innerPort}</td>
						<td align="center">${c.cpuShare}</td>
						<td align="center">${c.memLimit}</td>
						<td align="center"><a href="javascript:void(0)"
							onclick="window.open('${c.accessUrl}')">${c.accessUrl}</a></td>
						<td align="center">停止 删除</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<!-- 监控 -->
		<div style="width: 100%; margin: 0 auto;">
			<h3>Docker容器监控</h3>
		</div>
		<div id="stats"
			style="width: 98%; height: 300px; background-color: #000; color: #fff; margin: 0 auto; padding: 20px; font-size: 18px">
		</div>
	</div>
	<!-- 弹窗 -->
	<div id="form" class="div_content"
		style="display: none; background-color: #f2f2f2; width: 500px; margin: 0 auto; padding: 20px; margin-top: 20px; text-align: center;">
		<form method="post" id="addContainerForm"
			enctype="multipart/form-data">
			<h3>添加容器</h3>
			<table class="table1" style="width: 100%">
				<tr>
					<td align="right">Tomcat版本：</td>
					<td><select name="tomVersion" id="tomVersion"
						style="width: 250px">
							<option value="tomcat:latest">tomcat:latest</option>
							<option value="tomcat:9.0-jre8">tomcat:9.0-jre8</option>
							<option value="tomcat:8.0-jre8">tomcat:8.0-jre8</option>
							<option value="tomcat:7.0-jre8">tomcat:7.0-jre8</option>
					</select>
				</tr>
				<tr>
					<td align="right">CPU权重：</td>
					<td><select name="cpu" id="cpu"
						style="width: 250px">
							<option value="1024">默认</option>
							<option value="512">512</option>
							<option value="128">256</option>
							<option value="128">128</option>
					</select>
				</tr>
				<tr>
					<td align="right">内存限额：</td>
					<td><select name="mem" id="mem"
						style="width: 250px">
							<option value="1024">1G</option>
							<option value="512">512M</option>
							<option value="256">256M</option>
					</select>
				</tr>
				<tr>
					<td align="right">上传应用 ：</td>
					<td align="left"><input type="file" name="war" id="war" /></td>
				</tr>
			</table>
		</form>
		<button id="close">关闭</button>
		&nbsp;&nbsp;
		<button id="add">创建</button>
	</div>
</body>
</html>
