package com.fantasy.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * spring boot 启动程序
 * 
 * @author 公众号：18岁fantasy
 * @date 2019年6月13日 下午5:12:09
 */
@SpringBootApplication
public class ApplicationStart extends SpringBootServletInitializer {


  public static void main(String[] args) throws Exception {
    SpringApplication.run(ApplicationStart.class, args).start();
    // 默认端口8080 path为：“/”
  }
}
