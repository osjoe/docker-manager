package com.fantasy.docker.mvc;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ContainerPort;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DockerClientBuilder;

/**
 * 操作docker的客户端
 * 
 * @author 公众号：18岁fantasy
 * @date 2019年6月12日 下午4:18:53
 */
@Component
public class DockerClientOperaterServer implements InitializingBean {


  /**
   * spring 配置文件注入，默认从application.properties 中获取
   */
  @Value("${docker.url}")
  private String dockerUrl;
  /**
   * 客户端
   */
  private DockerClient dockerClient;
  /**
   * docker 容器列表缓存
   */
  private static final Map<String, ContainerVO> cs = new HashMap<String, ContainerVO>();

  /**
   * docker 基本信息
   */
  private static DockerVo dockerVo;

  /**
   * 容器监控信息
   */
  private static String stats;

  /**
   * 监控线程是否启动
   */
  private static Boolean statsbegin = false;

  /**
   * 初始化docker客户端链接
   * 
   * @param dockerUrl
   */
  public void initClient(String dockerUrl) {
    if (dockerClient == null) {
      System.out.println("初始化docker连接:" + dockerUrl);
      this.dockerClient = DockerClientBuilder.getInstance(dockerUrl).build();
    }
    stat();
    refreshContainers();

  }

  /**
   * 获取docker服务基本信息
   */
  public DockerVo getDockerInfo() {

    if (dockerVo == null) {
      dockerVo = new DockerVo();
      Info info = dockerClient.infoCmd().exec();
      System.out.println("info:" + info);
      dockerVo.setTotalMem(info.getMemTotal());
      dockerVo.setImages(info.getImages());
      dockerVo.setCpus(info.getNCPU());
    }
    return dockerVo;
  }

  /**
   * 获取监控信息
   * 
   * @return
   */
  public String getStatStr() {
    return stats;
  }

  /**
   * 获取监控信息
   * 
   * @param containtId 容器id或者容器名称
   */
  public void stat() {
    if (!statsbegin) {
      synchronized (statsbegin) {
        new Thread(new Runnable() {

          public void run() {
            try {
              System.out.println("开始监控程序...");
              String statCmd = "docker stats ";
              BufferedReader br = new BufferedReader(
                  new InputStreamReader(Runtime.getRuntime().exec(statCmd).getInputStream()));
              // StringBuffer b = new StringBuffer();
              String line = null;
              StringBuffer b = new StringBuffer();
              while ((line = br.readLine()) != null) {
                if (line.indexOf("NAME") != -1 && b.length() != 0) {
                  stats = b.toString();
                  System.out.println("监控结果：" + stats);
                  b.delete(0, b.length());
                  b.append(
                      "<span style='background-color: #0033dd;font-size:20px'>CONTAINER ID        NAME             CPU %           MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O     PIDS</span><br/>");
                } else {
                  b.append(line + "<br/>");
                }

              }
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }).start();
        statsbegin = true;
      }
    }
    // 也可以用docker-java接口获取，但是需要自行解析和计算百分比比较繁琐
    /*
     * dockerClient.statsCmd(containtId).exec(new ResultCallback<Statistics>() {
     * 
     * public void close() throws IOException { } public void onStart(Closeable closeable) { }
 
       * public void onNext(Statistics object) { System.out.println("######"+object);
       * //sts.put(containtId, object.) } public void onError(Throwable throwable) { } public
       * void onComplete() {
       * 
       * }});
       */
  }

  /**
   * 初始化docker链接
   * 
   * @param dockerUrl
   */
  public void initClient() {
    initClient(dockerUrl);
  }

  /**
   * 获取容器列表
   * 
   * @return
   */
  public List<ContainerVO> getContainers() {
    List<ContainerVO> containerVOs = new ArrayList<ContainerVO>();
    for (Iterator<ContainerVO> iterator = cs.values().iterator(); iterator.hasNext();) {
      containerVOs.add(iterator.next());
    }
    return containerVOs;
  }

  /**
   * 将上传到主机的app复制到tomcat，让tomcat加载
   * 
   * @param containerId
   * @param appRootPath
   */
  public void pushAppToCotainer(String containerId, String appRootPath) {
    dockerClient.copyArchiveToContainerCmd(containerId).withHostResource(appRootPath)
        .withRemotePath("/usr/local/tomcat/webapps").exec();
  }

  /**
   * 获取tomcat应用的缓存地址
   * 
   * @param containerId
   * @param appName
   * @return
   */
  public String getAppAccessUrl(String containerId, String appName) {
    ContainerVO containerVO = cs.get(containerId);
    return "http://localhost:" + containerVO.getPubPort() + "/" + appName;
  }

  /**
   * 刷新容器列表
   */
  private void refreshContainers() {
    List<Container> dockerSearch = dockerClient.listContainersCmd().exec();
    for (Iterator<Container> iterator = dockerSearch.iterator(); iterator.hasNext();) {
      Container container = (Container) iterator.next();
      ContainerPort[] containerPort = container.getPorts();
      // 获取容器详细信息
      InspectContainerResponse containerInfo =
          dockerClient.inspectContainerCmd(container.getId()).exec();
      ContainerVO containerVO = new ContainerVO();
      containerVO.setName(containerInfo.getName());
      containerVO.setId(containerInfo.getId());
      containerVO.setImageName(container.getImage());
      containerVO.setInnerPort(containerPort[0].getPrivatePort());
      containerVO.setPubPort(containerPort[0].getPublicPort());
      containerVO.setCpuShare(containerInfo.getHostConfig().getCpuShares());
      containerVO.setMemLimit(containerInfo.getHostConfig().getMemory());
      containerVO.setCreateTime(containerInfo.getCreated());
      containerVO.setStatus(containerInfo.getState().getStatus());

      if (!cs.containsKey(container.getId())) {
        cs.put(container.getId(), containerVO);
      }
    }
  }

  /**
   * 创建并启动容器,同时部署应用
   */
  public String createAndStartrContainerAnddeployApp(String tomVersion, File appLocalFile,
      Integer cpu, Long mem) {

    String containerId = createContainer(tomVersion, cpu, mem).getId();
    startContainer(containerId);
    pushAppToCotainer(containerId, appLocalFile.getAbsolutePath());
    String appName = appLocalFile.getName().split("\\.")[0];
    ContainerVO containerVO = cs.get(containerId);
    String containerName = containerVO.getName();
    String accessUrl = getAppAccessUrl(containerId, appName);
    containerVO.setAccessUrl(accessUrl);
    containerVO.setCpuShare(cpu);
    containerVO.setMemLimit(mem);
    return containerName;
  }

  /**
   * 创建并启动容器
   */
  public void createAndStartrContainer(String tomVersion, Integer cpu, Long mem) {
    startContainer(createContainer(tomVersion, cpu, mem).getId());
  }

  /**
   * 创建容器
   * 
   * @return
   */
  public CreateContainerResponse createContainer(String tomVersion, Integer cpuShares,
      Long memory) {
    ExposedPort tcp8080 = ExposedPort.tcp(8080);

    Ports portBindings = new Ports();
    portBindings.bind(tcp8080, Ports.Binding.empty());
    String containerName = "app" + RandomUtils.nextInt();

    CreateContainerResponse newContainer = dockerClient.createContainerCmd(tomVersion)// 镜像
        .withName(containerName)// 容器名称
        .withExposedPorts(tcp8080).withPortBindings(portBindings)// 端口绑定
        .withCpuShares(cpuShares).withMemory(memory * 1024 * 1024)// 限额,参数
        .exec();

    System.out.println("创建容器：" + containerName);

    return newContainer;
  }

  /**
   * 启动容器
   * 
   * @param containerId
   */
  public void startContainer(String containerId) {
    dockerClient.startContainerCmd(containerId).exec();
    refreshContainers();
    System.out.println("启动容器：" + cs.get(containerId).getName());
  }

  /**
   * 停止容器
   * 
   * @param containerId
   */
  public void stopContainer(String containerId) {
    dockerClient.stopContainerCmd(containerId).exec();
    System.out.println("停止容器：" + cs.get(containerId).getName());
    refreshContainers();
  }

  /**
   * 删除容器
   * 
   * @param containerId
   */
  public void rmContainer(String containerId) {
    dockerClient.removeContainerCmd(containerId).exec();
    System.out.println("删除容器：" + cs.get(containerId).getName());
    refreshContainers();
  }

  /**
   * 关闭docker连接
   * 
   * @param containerId
   * @throws IOException
   */
  public void closeClient() throws IOException {
    dockerClient.close();
  }

  /**
   * spring bean初始化后执行此方法，初始化docker连接
   */
  public void afterPropertiesSet() throws Exception {
    initClient();
  }

}
