package test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.fantasy.docker.mvc.DockerClientOperaterServer;

/**
 * 测试类
 * @author 公众号：18岁fantasy
 * @date 2019年6月13日 下午5:13:10
 */
public class Test {

  
  public static void main(String[] args) throws IOException {
    DockerClientOperaterServer clientOperater = new DockerClientOperaterServer();
    clientOperater.initClient("tcp://localhost:2375");

    //printList(clientOperater.getContainers());

    //clientOperater.createAndStartrContainer("tomcat:9.0-jre8",512,512l);
    //System.out.println(clientOperater.dockerInfo());
    clientOperater.stat();
    //printList(clientOperater.getContainers());
    System.in.read();
    //clientOperater.closeClient();
  }
  
  //----tools
  public static void printList(List<?> ls) {
    for (Iterator<?> iterator = ls.iterator(); iterator.hasNext();) {
      Object o = iterator.next();
      System.out.println(o);
    }
  }
}
